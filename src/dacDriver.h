/*
 * dacDriver.h
 *
 */

#ifndef DACDRIVER_H
#define DACDRIVER_H 1

#include "asynPortDriver.h"

/* These are the drvInfo strings that are used to identify the parameters.
 * They are used by asyn clients, including standard asyn device support */
#define P_DacRunString          "DAC_RUN"          /* asynInt32,            r/w */
#define P_Analog1String         "ANALOG_1"         /* asynFloat64Array,     r/w */
#define P_Analog2String         "ANALOG_2"         /* asynFloat64Array,     r/w */
#define P_Analog3String         "ANALOG_3"         /* asynFloat64Array,     r/w */
#define P_Analog4String         "ANALOG_4"         /* asynFloat64Array,     r/w */
#define P_Analog5String         "ANALOG_5"         /* asynFloat64Array,     r/w */
#define P_Analog6String         "ANALOG_6"         /* asynFloat64Array,     r/w */
#define P_Analog7String         "ANALOG_7"         /* asynFloat64Array,     r/w */
#define P_Analog8String         "ANALOG_8"         /* asynFloat64Array,     r/w */
#define P_DigitalAllString      "DIGITAL_ALL"      /* asynUInt32Digital,    r/w */

class dacDriver : public asynPortDriver {
public:
    dacDriver(int nsamples);

    virtual asynStatus writeInt32(asynUser *pasynUser, epicsInt32 value);
   
    virtual asynStatus writeFloat64Array(asynUser *pasynUser, epicsFloat64 *value,
                                         size_t nElements);
    virtual asynStatus readFloat64Array(asynUser *pasynUser, epicsFloat64 *value,
                                        size_t nElements, size_t *nIn);


    /* Not inherited from asynPortDriver */
    void simTask(void);

protected:
    /** Values used for pasynUser->reason, and indexes into the parameter library. */
    int P_DacRun;
    #define FIRST_SCOPE_COMMAND P_DacRun
    int P_Analog1;
    int P_Analog2;
    int P_Analog3;
    int P_Analog4;
    int P_Analog5;
    int P_Analog6;
    int P_Analog7;
    int P_Analog8;
    int P_DigitalAll;
    #define LAST_SCOPE_COMMAND P_DigitalAll

private:
    /* Our data */
    epicsFloat64 **analogDataIn;
    epicsFloat64 **analogDataOut;
    int nsamples;
    epicsEventId eventId;
};


#define NUM_SCOPE_PARAMS (&LAST_SCOPE_COMMAND - &FIRST_SCOPE_COMMAND + 1)

#endif /* dacDriver.h */

