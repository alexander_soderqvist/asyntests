/*
 * dacDriver.cpp
 */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <errno.h>
#include <math.h>
#include <unistd.h>

#include <epicsTypes.h>
#include <epicsTime.h>
#include <epicsThread.h>
#include <epicsString.h>
#include <epicsTimer.h>
#include <epicsMutex.h>
#include <epicsEvent.h>
#include <iocsh.h>

#include "dacDriver.h"
#include <epicsExport.h>


static const char *driverName="dacDriver";
void simTask(void *drvPvt);


/** Constructor for the dacDriver class.
 * Calls constructor for the asynPortDriver base class.
 */
dacDriver::dacDriver(int nsamples_in)
    : asynPortDriver("PORTYPORT",
            1, /* maxAddr */
            (int)NUM_SCOPE_PARAMS,
            asynInt32Mask | asynUInt32DigitalMask | asynFloat64ArrayMask | asynDrvUserMask, /* Interface mask */
            asynInt32Mask | asynUInt32DigitalMask | asynFloat64ArrayMask | asynGenericPointerMask,  /* Interrupt mask */
            0, /* asynFlags.  This driver does not block and it is not multi-device, so flag is 0 */
            1, /* Autoconnect */
            0, /* Default priority */
            0) /* Default stack size*/
{
    asynStatus status;
    const char *functionName = "dacDriver";
    nsamples = nsamples_in;
    eventId = epicsEventCreate(epicsEventEmpty);
    int i;

    /* Allocate the data arrays */
    analogDataIn = (epicsFloat64 **)calloc(8, sizeof(epicsFloat64*));
    analogDataOut = (epicsFloat64 **)calloc(8, sizeof(epicsFloat64*));
    for (i=0;i<8;++i) {
        analogDataIn[i]  = (epicsFloat64 *)calloc(nsamples, sizeof(epicsFloat64));
        if(analogDataIn[i] == NULL) {
            printf("%s:%s: calloc failed, you're out of memory =)\n",driverName,functionName);
            return;
        }
        analogDataOut[i]  = (epicsFloat64 *)calloc(nsamples, sizeof(epicsFloat64));
        if(analogDataOut[i] == NULL) {
            printf("%s:%s: calloc failed, you're out of memory =)\n",driverName,functionName);
            return;
        }
    }
    

    /* Bind params */
    createParam(P_DacRunString,         asynParamInt32,         &P_DacRun);
    createParam(P_DigitalAllString,     asynParamUInt32Digital, &P_DigitalAll);
    for(i=0;i<8;++i) {
        char res[10]; /* Temporary storage variable */
        /* Create params for Analog in/out */
        sprintf(res, "ANALOG_%d", i+1);
        createParam(res,      asynParamFloat64Array,  &P_Analog1+i);

        
    }

    setIntegerParam(P_DacRun, 0);

    setUIntDigitalParam(P_DigitalAll, (epicsUInt32)0, 0xff);


    /* Create the thread that computes the "analog in" in the background */
    status = (asynStatus)(epicsThreadCreate("dacDriverTask",
                epicsThreadPriorityMedium,
                epicsThreadGetStackSize(epicsThreadStackMedium),
                (EPICSTHREADFUNC)::simTask,
                this) == NULL);
    if (status) {
        printf("%s:%s: epicsThreadCreate failure\n", driverName, functionName);
        return;
    }
}



void simTask(void *drvPvt)
{
    dacDriver *pPvt = (dacDriver *)drvPvt;

    pPvt->simTask();
}

/** Simulation task that runs as a separate thread. */
void dacDriver::simTask(void)
{
    int i,j,run;

    epicsFloat64 * data = NULL;

    /* Loop forever */
    while (1) {
        getIntegerParam(P_DacRun, &run);
        if(!run) epicsEventWait(eventId);
        for (i=0; i<8; i++) {
            data = analogDataIn[i];
            for(j=0; j<nsamples; ++j){
                data[j] = rand()/(float)RAND_MAX;
            }
            doCallbacksFloat64Array(data, nsamples, *(&P_Analog1+i),0);
        }
        setIntegerParam(P_DacRun, 0);
        callParamCallbacks();
    }
}



asynStatus dacDriver::writeInt32(asynUser *pasynUser, epicsInt32 value)
{
    int function = pasynUser->reason;
    asynStatus status = asynSuccess;
    const char *paramName;
    const char* functionName = "writeInt32";

    /* Set the parameter in the parameter library. */
    status = (asynStatus) setIntegerParam(function, value);

    /* Fetch the parameter string name for possible use in debugging */
    getParamName(function, &paramName);

    if (function == P_DacRun) {
        /* If run was set then wake up the simulation task.
         * This is the reason we can't use the standard implementation
         * of writeInt32 */
        if (value) epicsEventSignal(eventId);
    }

    /* Do callbacks so higher layers see any changes */
    status = (asynStatus) callParamCallbacks();

    if (status)
        epicsSnprintf(pasynUser->errorMessage, pasynUser->errorMessageSize, 
                "%s:%s: status=%d, function=%d, name=%s, value=%d", 
                driverName, functionName, status, function, paramName, value);
    else
        asynPrint(pasynUser, ASYN_TRACEIO_DRIVER, 
                "%s:%s: function=%d, name=%s, value=%d\n", 
                driverName, functionName, function, paramName, value);
    return status;
}

asynStatus dacDriver::writeFloat64Array(asynUser *pasynUser, epicsFloat64 *value, size_t nElements)
{
    int function = pasynUser->reason;
    const char *paramName;
    const char *functionName = "writeFloat64Array";
    epicsFloat64 *data = NULL;

    /* Fetch the parameter string name for possible use in debugging */
    getParamName(function, &paramName);

    if(function == P_Analog1)
        data = analogDataOut[0];
    else if(function == P_Analog2)
        data = analogDataOut[1];
    else if(function == P_Analog3)
        data = analogDataOut[2];
    else if(function == P_Analog4)
        data = analogDataOut[3];
    else if(function == P_Analog5)
        data = analogDataOut[4];
    else if(function == P_Analog6)
        data = analogDataOut[5];
    else if(function == P_Analog7)
        data = analogDataOut[6];
    else if(function == P_Analog8)
        data = analogDataOut[7];

    memcpy(data, value, nElements*sizeof(epicsFloat64));

    asynPrint(pasynUser, ASYN_TRACEIO_DRIVER,
            "%s:%s: function=%d, name=%s\n",
            driverName, functionName, function, paramName);
    return asynSuccess;
}

/*
 * nElments: Number of elements requested
 * nIn:      Number of elements actually transferred
 */
asynStatus dacDriver::readFloat64Array(asynUser *pasynUser, epicsFloat64 *value,
        size_t nElements, size_t *nIn)
{
    int function = pasynUser->reason;
    size_t ncopy;
    const char *functionName = "readFloat64Array";
    epicsFloat64* data = NULL;

    if(function == P_Analog1)
        data = analogDataIn[0];
    else if(function == P_Analog2)
        data = analogDataIn[1];
    else if(function == P_Analog3)
        data = analogDataIn[2];
    else if(function == P_Analog4)
        data = analogDataIn[3];
    else if(function == P_Analog5)
        data = analogDataIn[4];
    else if(function == P_Analog6)
        data = analogDataIn[5];
    else if(function == P_Analog7)
        data = analogDataIn[6];
    else if(function == P_Analog8)
        data = analogDataIn[7];

    ncopy = nsamples;
    if (nElements < ncopy) ncopy = nElements;
    *nIn = ncopy;
    memcpy(value, data, ncopy*sizeof(epicsFloat64));

    asynPrint(pasynUser, ASYN_TRACEIO_DRIVER,
            "%s:%s: function=%d\n",
            driverName, functionName, function);
    return asynSuccess;
}


/* Configuration routine.  Called directly, or from the iocsh function below */

extern "C" {

    /** EPICS iocsh callable function to call constructor for the dacDriver class.*/
    int dacDriverConfigure(int nsamples)
    {
        new dacDriver(nsamples);
        return(asynSuccess);
    }


    /* EPICS iocsh shell commands */
    static const iocshArg initArg0 = { "nsamples", iocshArgInt};
    static const iocshArg * const initArgs[] = {&initArg0};
    static const iocshFuncDef initFuncDef = {"dacDriverConfigure",1,initArgs};
    static void initCallFunc(const iocshArgBuf *args)
    {
        dacDriverConfigure(args[0].ival);
    }

    void dacDriverRegister(void)
    {
        iocshRegister(&initFuncDef,initCallFunc);
    }

    epicsExportRegistrar(dacDriverRegister);

}

/* vim: set sw=4 sts=4: */
